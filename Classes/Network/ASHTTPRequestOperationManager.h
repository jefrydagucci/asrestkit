//
//  ASHTTPRequestOperationManager.h
//  ASRestKit
//
//  Created by Jefry Da Gucci on 2/23/15.
//  Copyright (c) 2015 Appslon http://appslon.com. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//

#import "ASSharedObject.h"
#import "RKHTTPUtilities.h"

@class
ASObjectRequestOperation,
ASManagedObjectRequestOperation,
RKMappingResult,
RKManagedObjectStore;

@interface ASHTTPRequestOperationManager : ASSharedObject

#pragma mark - ASObjectRequestOperation operation
/**
 Adding Object request operation to operation queue.
 @param operation Operation will be added to queue.
 @param success The block to be executed on the completion of a successful operation. This block has no return value and takes two arguments: the receiver operation and the mapping result from object mapping the response data of the request.
 @param failure The block to be executed on the completion of an unsuccessful operation. This block has no return value and takes two arguments: the receiver operation and the error that occurred during the execution of the operation.
 */
+ (void)addObjectRequestOperation:(ASObjectRequestOperation *)operation
                   operationQueue:(NSOperationQueue *)operationQueue
                          success:(void(^)(ASObjectRequestOperation *operation,
                                           RKMappingResult *result))success
                          failure:(void(^)(ASObjectRequestOperation *operation,
                                           NSError *error))failure;


#pragma mark - ASManagedObjectRequestOperation operation
/**
 Adding Object request operation to operation queue.
 @param operation Operation will be added to queue.
 @param managedObjectStore managed object store used to store the object.
 @param success The block to be executed on the completion of a successful operation. This block has no return value and takes two arguments: the receiver operation and the mapping result from object mapping the response data of the request.
 @param failure The block to be executed on the completion of an unsuccessful operation. This block has no return value and takes two arguments: the receiver operation and the error that occurred during the execution of the operation.
 */
+ (void)addManagedObjectRequestOperation:(ASManagedObjectRequestOperation *)operation
                      managedObjectStore:(RKManagedObjectStore *)managedObjectStore
                          operationQueue:(NSOperationQueue *)operationQueue
                                 success:(void(^)(ASManagedObjectRequestOperation *operation,
                                                  RKMappingResult *result))success
                                 failure:(void(^)(ASManagedObjectRequestOperation *operation,
                                                  NSError *error))failure;

@end
