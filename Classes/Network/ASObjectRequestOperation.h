//
//  ASObjectRequestOperation.h
//  ASRestKit
//
//  Created by Jefry Da Gucci on 2/23/15.
//  Copyright (c) 2015 Appslon http://appslon.com. All rights reserved.
//
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//

#import "RKObjectRequestOperation.h"

@interface ASObjectRequestOperation : RKObjectRequestOperation

#pragma mark - instance
/**
 Basic HTTP object request operation using `RestKit`
 @param baseURL base URL of HTTP request.
 @param pathPattern path pattern for HTTP request. e.g "http://appslon.com/api/v1/method" will have "/api/v1/method" as path pattern.
 @param params parameters will be sent to server.
 @param requestMethod is request method will be used. e.g: RKRequestMethodGET.
 @param keyPath the key path of the mapped object will be requested.
 @param timeoutInterval time out interval will be used when trying to hit the server.
 
 @return instance for managedobject request operation
 */
- (instancetype)initWithBaseURL:(NSURL *)baseURL
                        mapping:(RKObjectMapping *)requestMapping
                    pathPattern:(NSString *)pathPattern
                         params:(NSDictionary *)params
                         method:(RKRequestMethod)requestMethod
                        keyPath:(NSString *)keyPath
                timeoutInterval:(NSTimeInterval)timeoutInterval;;

@end
