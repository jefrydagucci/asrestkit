//
//  ASHTTPRequestOperationManager.m
//  ASRestKit
//
//  Created by Jefry Da Gucci on 2/23/15.
//  Copyright (c) 2015 Appslon http://appslon.com. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//

#import "ASHTTPRequestOperationManager.h"
#import "ASObjectRequestOperation.h"
#import "ASManagedObjectRequestOperation.h"

@implementation ASHTTPRequestOperationManager

#pragma mark - ASObjectRequestOperation operation

+ (void)addObjectRequestOperation:(ASObjectRequestOperation *)operation
                   operationQueue:(NSOperationQueue *)operationQueue
                          success:(void(^)(ASObjectRequestOperation *operation,
                                           RKMappingResult *result))success
                          failure:(void(^)(ASObjectRequestOperation *operation,
                                           NSError *error))failure{
    
    [operation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *result) {
        
        !success?:success((ASObjectRequestOperation *)operation, result);
        
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        
        !failure?:failure((ASObjectRequestOperation *)operation, error);
        
    }];
    
    [operationQueue addOperation:operation];
}


#pragma mark - ASManagedObjectRequestOperation operation

+ (void)addManagedObjectRequestOperation:(ASManagedObjectRequestOperation *)operation
                      managedObjectStore:(RKManagedObjectStore *)managedObjectStore
                          operationQueue:(NSOperationQueue *)operationQueue
                                 success:(void(^)(ASManagedObjectRequestOperation *operation,
                                                  RKMappingResult *result))success
                                 failure:(void(^)(ASManagedObjectRequestOperation *operation,
                                                  NSError *error))failure{
    
    [operation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *result) {
        
        !success?:success((ASManagedObjectRequestOperation *)operation, result);
        
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        
        !failure?:failure((ASManagedObjectRequestOperation *)operation, error);
        
    }];
    
    [operationQueue addOperation:operation];
}

@end
