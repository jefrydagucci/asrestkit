
Pod::Spec.new do |s|

  s.name          = "ASRestKit"
  s.version       = "0.9.1"
  s.summary       = "ASRestKit is a collection of codes can be used as base of ios project using RestKit"

  s.description   = <<-DESC
  ASRestKit is a collection of codes containing some customization use of RestKit classes
                   DESC

  s.homepage      = "http://appslon.com"

  s.license       = { :type => "Apache License, Version 2.0", :file => "LICENSE" }

  s.author            = { "jefrydagucci" => "jefrydagucci@gmail.com" }
  s.social_media_url  = "http://twitter.com/jefrydagucci"

  s.platform       = :ios
  s.ios.deployment_target = "7.0"

  s.source        = { :git => "https://bitbucket.org/jefrydagucci/asrestkit.git", :tag => "v#{s.version}" }

  s.source_files  = "Classes", "Classes/**/*.{h,m}"

  s.requires_arc  = true

  s.dependency       'ASBaseIOSProject', '~> 0.9.4'
  s.dependency       'RestKit', '~> 0.24.0'

  s.ios.frameworks    = 'SystemConfiguration', 'MobileCoreServices'

  s.prefix_header_contents = <<-EOS

#import <Availability.h>
#define _AFNETWORKING_PIN_SSL_CERTIFICATES_
#if __IPHONE_OS_VERSION_MIN_REQUIRED
  #import <SystemConfiguration/SystemConfiguration.h>
  #import <MobileCoreServices/MobileCoreServices.h>
  #import <Security/Security.h>
#else
  #import <SystemConfiguration/SystemConfiguration.h>
  #import <CoreServices/CoreServices.h>
  #import <Security/Security.h>
#endif

EOS

end
