//
//  main.m
//  ASRestKit
//
//  Created by Jefry Da Gucci on 2/23/15.
//  Copyright (c) 2015 Appslon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
