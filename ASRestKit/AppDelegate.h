//
//  AppDelegate.h
//  ASRestKit
//
//  Created by Jefry Da Gucci on 2/23/15.
//  Copyright (c) 2015 Appslon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

